# Vadd_A_B

This project elaborates the use of Vitis environment to perform 1D vector addition using RTL kernel interface.

## Installation

[Vitis 2019.2](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis.html).

## Usage

1. ./build.sh
2. cp host xclbin
3. export XCL_EMULATION_MODE=hw_emu
4. ./host *.xclbin

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
